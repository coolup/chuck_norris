import React from 'react';
import './App.css';
import Fulljokes from './containers/Fulljokes';

function App() {
  return (
    <div className="App">
      <Fulljokes/>
    </div>
  );
}

export default App;
