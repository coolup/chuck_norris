import React, {Component} from 'react'
import './Joke.css'

class Joke extends Component{
    
    componentWillMount(){
        console.log('[Joke] Will Mount');
    }
    componentDidMount(){
        console.log('[Joke] Did Mount');
    }
    componentWillUpdate() {
        console.log('[Joke] Will Update');
    }
    shouldComponentUpdate(nextProps, nextState){
        console.log('[Joke] ShouldUpdate')
        return nextProps.url !== this.props.url || 

        nextProps.value !== this.props.value;
    }
    render(){
        return(
            <div className="Joke" onClick={this.props.clicked}>
                <h1>Jokes about Chuck Norris</h1>                
                
                <div><img src={this.props.icon_url}></img></div>                
                <div className="Joketext">{this.props.fulljokes}</div>
               
            </div>
        )
    }
} 

export default Joke;