import React, { Component, Fragment } from 'react'
import './Fulljokes.css'
import Joke from '../components/Joke'

class Fulljokes extends Component {
    
    
     gettingFulljokes = async () =>{
         const FulljokesArray =[];
         for(let i = 0; i < 5; i++){
            const api_url = await fetch('https://api.chucknorris.io/jokes/random/');
            const data = await api_url.json();
            FulljokesArray.push(data)
            console.log(data)
         }
       this.setState({fulljokes: FulljokesArray})

    }
    componentDidMount () {
       this.gettingFulljokes();
    }
    state = {
        fulljokes: []        
    }   
    render() {
        return (
            <Fragment>
                <section className="Fulljokes">
                    {this.state.fulljokes.map(joke=>(
                        <Joke 
                            key={joke.id} 
                            fulljokes={joke.value}
                            icon_url={joke.icon_url}
                        />
                    ))}           
                </section>
                <button className="ToggleButton" onClick={this.gettingFulljokes}> New Jokes</button>               
                
            </Fragment>
        )
    }
}

export default Fulljokes;